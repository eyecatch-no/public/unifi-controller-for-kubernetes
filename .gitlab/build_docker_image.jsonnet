local versions = import '../chartversions.libsonnet';

local job(tag, chartVersion) =
{
  image: 'docker.io/eyecatch/oci-builder:buildah',
  before_script: [
    'buildah login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com',
    'buildah login -u $CI_DEPENDENCY_PROXY_USER -p $CI_DEPENDENCY_PROXY_PASSWORD $CI_DEPENDENCY_PROXY_SERVER'
  ],
  script: [
    'buildah build --platform linux/amd64 --build-arg UNIFI_TAG=$UNIFI_TAG --tag "${IMAGE_NAME}:$UNIFI_TAG" -f ./container/Dockerfile ./container',
    'buildah push ${IMAGE_NAME}:$UNIFI_TAG'
  ],
  variables: {
      UNIFI_TAG: tag
  }
};

function(package_version) {
  ['job/' + package_version + '/' + x]: job(x, package_version)
  for x in versions
}