local versions = import '../chartversions.libsonnet';

local job(appVersion, chartVersion) =
{
  image: 'registry.gitlab.com/eyecatch-no/common/gitlab-build-helper-image/amd64',
  script: [
    'helm package . --app-version $APP_VERSION --version $CHART_VERSION',
    'curl --request POST --user gitlab-ci-token:$CI_JOB_TOKEN --form "chart=@unifi-controller-$CHART_VERSION.tgz" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/api/stable/charts"'
  ],
  variables: {
      APP_VERSION: appVersion,
      CHART_VERSION: chartVersion + '+' + appVersion
  },
  artifacts: {
    paths: [
      'unifi-controller-$CHART_VERSION.tgz'
    ]
  }
};

function(package_version) {
  ['job/' + package_version + '/' + x]: job(x, package_version)
  for x in versions
}
