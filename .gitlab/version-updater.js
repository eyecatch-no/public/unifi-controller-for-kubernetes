const YAML = require('yaml')

module.exports.readVersion = function (contents) {
    return YAML.parse(contents).version
}

module.exports.writeVersion = function (contents, version) {
    const yaml = YAML.parseDocument(contents); 
    yaml.set('version', version);
    
    return yaml.toString();
};