const trackers = [{
  filename: "Chart.yaml",
  updater: require("./.gitlab/version-updater.js")
},
{
  filename: "package.json",
  type: "json"
}];

const config = {
  packageFiles: trackers,
  bumpFiles: [
    ...trackers,
    {
      filename: "VERSION",
      type: "plain-text"
    }
  ]
}

module.exports = config;