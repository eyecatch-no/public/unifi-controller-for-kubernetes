# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [0.1.8](https://gitlab.com/eyecatch-no/public/unifi-controller-for-kubernetes/compare/v0.1.7...v0.1.8) (2023-06-16)


### Features

* Added support for specifying a custom storage class ([36af155](https://gitlab.com/eyecatch-no/public/unifi-controller-for-kubernetes/commit/36af1554af763f475237e3590b299f209e42990e))

### [0.1.7](https://gitlab.com/eyecatch-no/public/unifi-controller-for-kubernetes/compare/v0.1.6...v0.1.7) (2022-09-29)


### Features

* Added support for specifying a custom storage class ([1c171dd](https://gitlab.com/eyecatch-no/public/unifi-controller-for-kubernetes/commit/1c171dd813e06a958222fe15b14a1020dcf029cc))

### [0.1.6](https://gitlab.com/eyecatch-no/public/unifi-controller-for-kubernetes/compare/v0.1.5...v0.1.6) (2022-09-02)


### Features

* Added support for mongodb connection string arguments ([e63a3f9](https://gitlab.com/eyecatch-no/public/unifi-controller-for-kubernetes/commit/e63a3f929fbab055a257f9125ec09728698a5bf2))

### [0.1.5](https://gitlab.com/eyecatch-no/public/unifi-controller-for-kubernetes/compare/v0.1.4...v0.1.5) (2022-09-02)
> N/A

### [0.1.4](https://gitlab.com/eyecatch-no/public/unifi-controller-for-kubernetes/compare/v0.1.3...v0.1.4) (2022-09-02)
> N/A

### [0.1.3](https://gitlab.com/eyecatch-no/public/unifi-controller-for-kubernetes/compare/v0.1.0...v0.1.3) (2022-09-02)
> N/A
