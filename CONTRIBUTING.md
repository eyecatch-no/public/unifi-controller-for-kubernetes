# How to Contribute

## Contributor Behavior
All contributors MUST adhere to the [Code of Conduct](./CODE_OF_CONDUCT.md)

## Pull Requests

This project uses the [GitHub flow](https://guides.github.com/introduction/flow/) as main versioning workflow

1. Fork the repository
2. Create a new branch for each feature, fix or improvement
3. Send a pull request from each feature branch to the **main** branch

It is very important to separate new features or improvements into separate feature branches, and to send a
pull request for each branch.

This allow us to review and pull in new features or improvements individually.

## Style Guide

All pull requests messages MUST adhere to the [Conventional Commits specification](https://conventionalcommits.org/).  
The commits in your own branch will be squashed, and only the pull request message will remain.
