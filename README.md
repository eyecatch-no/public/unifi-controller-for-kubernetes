# Unifi Controller for Kubernetes

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

> In Progress!  
> Not suitable for use yet

## Introduction
### Helm Chart
Helm Chart for running a Unifi Controller on Kubernetes.  

Features:  
- [x] Support for external and internal MongoDB instances
- [x] Automatic certificate generation via Lets Encrypt

### Docker Image
Based on the excellent [unifi-docker container](https://github.com/jacobalberty/unifi-docker).  
Adds support for automatic SSL certificate generation.

## Usage

[Helm](https://helm.sh) must be installed to use the charts.
Please refer to Helm's [documentation](https://helm.sh/docs/) to get started.

Once Helm is set up properly, add the repo as follows:

```bash
helm repo add unifi-controller https://gitlab.com/api/v4/projects/35990914/packages/helm/stable
helm repo update
```

Then you can install the repository

```bash
helm install unifi-controller/unifi-controller
```

### Certificates
If you are using auto-generated DNS entries or the hostname is otherwise not available until _after_ the chart is installed, the certificate generation will fail.  
To remedy this, restart the pod after the DNS is properly configured: `kubectl delete pod unifi-controller-0`. When it starts up afterwards, it should generate the certificate properly.

### Ingress
If you are using AKS, you can specify the first part of the DNS name of the service by setting the following annotations:
```yaml
annotations:
    service.beta.kubernetes.io/azure-dns-label-name: my-unifi-public-hostname
    service.beta.kubernetes.io/azure-pip-tags: tag1=value1,tag2=value2
```

### MongoDB Atlas
If you are running the MongoDB on Atlas, you need to create a user with the following rights to use with Unifi

```
dbAdmin@{mainDb}
dbAdmin@{statDb}
read@admin."system.version"
readWrite@{mainDb}
readWrite@{statDb}
```

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| timezone | `string | `Europe/Oslo` | The time zone the controller should use |
| certificate.enabled | bool | `false` | Generate a certificate using Lets Encrypt. Note that ingress handling for validation must be set up, it is not covered by this helm chart. |
| certificate.email | string | `` | The email address that should be used when generating certificate |
| certificate.hostname | string | `` | The host name that should be used when generating certificate |
| mongodb.create | bool | `true` | Set up an internal MongoDB server to use for the controller |
| mongodb.image | string | `mongo` | The mongodb docker image to use |
| mongodb.tag | string | `5.0.11-focal` | The mongodb tag to use. NOTE: Unifi v6.5.55 does not support MongoDB v6.x |
| mongodb.replicas | number | `mongo` | The number of replicas to use for the MongoDB instance. NOTE: More than 1 replica has not been tested. |
| mongodb.username | string | `unifi` | The MongoDB instance username |
| mongodb.password | string | `unifi` | The MongoDB instance password. Use `mongodb.passwordSecret` if you don't want to have this in cleartext |
| mongodb.passwordSecret | string | `` | The name of the secret to use for the MongoDB instance password. The password must be stored in a property called `password` |
| mongodb.resources | object | `{}` | The [resources](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/) to use for the MongoDB instance |
| mongodb.mainDbName | string | `unifi` | The name of the main Unifi database |
| mongodb.statDbName | string | `unifi_stat` | The name of the statistics Unifi database |
| mongodb.authDbName | string | `admin` | The database where the user is created |
| mongodb.connectionString | string | `` | A custom connection string. Use this if you are using an external MongoDB instance |
| mongodb.connectionStringArguments | list | `[]` | A set of arguments to append to the connection string |
| serviceAccount.create | bool | `` | Create a separate service account for the services in this helm chart |
| serviceAccount.annotations | object | `{}` | [Annotations](https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/) for the service account |
| serviceAccount.name | string | `` | The name of the service account |
| podAnnotations | object | `{}` | [Annotations](https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/) for the pod |
| podSecurityContext | object | `{}` | The [pod security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/). |
| service.type | string | `LoadBalancer` | The [service type](https://kubernetes.io/docs/concepts/services-networking/service/#publishing-services-service-types). Use `ClusterIP` if you dont want to expose this externally, or you're using your own ingress/reverse proxy |
| service.annotations | object | `{}` | [Annotations](https://kubernetes.io/docs/concepts/overview/working-with-objects/annotations/) for the service. Use this to add custom directives to a hosting provider like AKS. |
| persistence.enabled | bool | `true` | This is the only valid value for now |
| persistence.size | string | `64Gi` | The size of the Unifi controller storage |
| controller.image.repository | string | `registry.gitlab.com/eyecatch-no/public/unifi-controller-for-kubernetes/k8s-unifi-controller` | The image to use for the controller. |
| controller.image.pullPolicy | string | `Always` | The [image pull policy](https://kubernetes.io/docs/concepts/containers/images/#image-pull-policy) |
| controller.livenessProbe | object | `{"livenessProbe":{"httpGet":{"path":"/","port":8080},"failureThreshold":3,"timeoutSeconds":30,"initialDelaySeconds":300,"periodSeconds":30}}` | [The liveness probe](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#define-a-liveness-command) |
| controller.readinessProbe | object | `{"readinessProbe":{"httpGet":{"path":"/","port":8080},"failureThreshold":3,"timeoutSeconds":5,"initialDelaySeconds":60,"periodSeconds":5}}` | [The readiness probe](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#define-readiness-probes) |
| controller.securityContext | object | `{}` | The controller [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/). |
| controller.resources | object | `{}` | The [resources](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/) to use for the controller. |
| log.resources | object | `{}` | The [resources](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/) to use for the log container. |
| nodeSelector | object | `{}` | [Assign the pods to specific nodes](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#nodeselector) |
| tolerations | object | `{}` | [Schedule the pods to nodes with specific taints](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/ |
| affinity | object | `{}` | [Prefer specific node types for the pods](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#affinity-and-anti-affinity) |

## Contributing

We'd love to have you contribute! Please refer to our [contribution guidelines](./CONTRIBUTING.md) for details.
