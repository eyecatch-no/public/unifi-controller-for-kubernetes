#!/usr/bin/env bash
if [ -n "$UNIFI_HOSTNAME" ]; then
    mkdir -p /unifi/cert;
    certbot certonly --standalone --non-interactive --agree-tos -m ${CERT_EMAIL} -d ${UNIFI_HOSTNAME} || true;
    ln -s /etc/letsencrypt/live/${UNIFI_HOSTNAME}/fullchain.pem /unifi/cert/fullchain.pem || true;
    ln -s /etc/letsencrypt/live/${UNIFI_HOSTNAME}/cert.pem /unifi/cert/cert.pem || true;
    ln -s /etc/letsencrypt/live/${UNIFI_HOSTNAME}/chain.pem /unifi/cert/chain.pem || true;
    ln -s /etc/letsencrypt/live/${UNIFI_HOSTNAME}/privkey.pem /unifi/cert/privkey.pem || true;
fi
/usr/local/bin/docker-entrypoint.sh unifi