#!/bin/sh
apk update
apk add --no-cache inotify-tools
echo "Watching logs: $1"
while true; do
    ( find $1 -type f -print0 | xargs -0 tail -f -v -n 0 )&
    tailPid=$!
    inotifywait --recursive --quiet --event create --format=%w%f $1 | tr \\n \\0 | xargs -0 tail -v
    kill $tailPid
done