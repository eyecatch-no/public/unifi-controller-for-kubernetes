{{/*
Expand the name of the chart.
*/}}
{{- define "common.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "common.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "common.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "common.labels" -}}
helm.sh/chart: {{ include "common.chart" . }}
{{ include "common.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Controller selector labels
*/}}
{{- define "common.selectorLabels" -}}
app.kubernetes.io/name: {{ include "common.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "common.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "common.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

#####

{{/*
MongoDB Selector labels
*/}}
{{- define "mongodb.selectorLabels" -}}
app.kubernetes.io/name: {{ include "common.name" . }}-mongodb
app.kubernetes.io/instance: {{ .Release.Name }}-mongodb
{{- end }}

{{/*
MongoDB Connection String
*/}}
{{- define "mongodb.connectionString" -}}
{{- if .Values.mongodb.connectionString }}
{{- .Values.mongodb.connectionString }}
{{- else }}
{{- printf "mongodb://%s:%s@%s-mongodb.%s:27017" .Values.mongodb.username (include "mongodb.password" .) (include "common.name" .) .Release.Namespace }}
{{- end }}
{{- end }}

{{/*
MongoDB Main DB URI
*/}}
{{- define "mongodb.dbUri" -}}
{{ printf "%s/%s?authSource=%s&%s" (include "mongodb.connectionString" .) (.Values.mongodb.mainDbName | default "unifi") .Values.mongodb.authDbName (join "&" .Values.mongodb.connectionStringArguments) }} 
{{- end }}

{{/*
MongoDB Stat DB URI
*/}}
{{- define "mongodb.statDbUri" -}}
{{ printf "%s/%s?authSource=%s&%s" (include "mongodb.connectionString" .) (.Values.mongodb.statDbName | default "unifi_stat") .Values.mongodb.authDbName (join "&" .Values.mongodb.connectionStringArguments) }} 
{{- end }}

{{/*
MongoDB Password
*/}}
{{- define "mongodb.password" -}}
{{- if .Values.mongodb.passwordSecret }}
{{- (lookup "v1" "Secret" .Release.Namespace .Values.mongodb.passwordSecret).data.password }}
{{- else }}
{{- .Values.mongodb.password }}
{{- end }}
{{- end }}